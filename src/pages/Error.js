import Banner from '../components/Banner';

const Error = () => {


   let errorBanner = {


       title: "Page Not Found",

       content: "The page you're looking for does not exist.",

       label: "Back to Home",

       destination: "/"


   }


   return <Banner data={errorBanner} />

}

export default Error;